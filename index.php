
<!doctype html>
<head>
    <title>Sample Information System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style>
    .search {
        width: 60%;
        margin-left: 20%;
        padding:10px 10px;
    }
    .head {
        text-align: center;
        padding: 20px 10px;
        background-color: blue;
        color: white;
    
    }
    .head2 {
        text-align: center;
        padding: 20px 10px;
    
    }
    .myform {
        text-align:center;
        width: 50%;
        margin-left:25%;
        margin-top: 100px;
        font-weight: bold;
    }
    .myform2 {
        text-align:center;
        width: 80%;
        margin-left:10%;
        margin-top: 100px;
        margin-bottom: 200px;
        font-weight: bold;
    }
    .login {
        padding: 10px 10px;
        background-color: green;
        color: white;
        width: 200px;
    }
    .delete {
        padding: 10px 10px;
        background-color: red;
        color: white;
        width: 200px;
    }
    .edit {
        padding: 10px 10px;
        background-color: green;
        color: orange;
        width: 200px;
    }
    .list {
        text-decoration: none;
    }
    #myUL {
      width: 60%;
      margin-left:20%;
      font-weight: bold;
      margin-bottom: 200px;
    }
    </style>
    
</head>

<body>
    <h1 class="head">Attendance Login System</h1>
    <form class="myform" action="login.php" method="POST">
        <p>First Name:
            <input type="text" name="fname" placeholder="Enter first name">
        </p>
        <p> Last Name:
            <input type="text" name="lname" placeholder="Enter last name">
        </p>
        <p>Gender:
            <select name="gender">
                <option value="male">MALE</option>
                <option value="female">FEMALE</option>
            </select>
        </p>
        <p>
            <input class="login" type="submit" value="LOG IN">
        </p>
    </form>   

    <hr>
    <h1 class="head2">USERS</h1>
    
    
    <p style="text-align:center">TABLE STYLE
</p>
    <table border="1" class="myform2">
        <tr>
        <th>USER ID</th>
            <th>FIRST NAME</th>
            <th>LAST NAME</th>
            <th>GENDER</th>
            <th>DATE LOGIN</th>
            <th>DATE LOGOUT</th>
            <th>INTERVAL TIME</th>
            <th>EDIT</th>
            <th>DELETE</th>
        </tr>

      <?php
      include 'connect.php';

      $selectquery2 = mysqli_query($conn, "SELECT * FROM user");

      while($row= mysqli_fetch_array($selectquery2)){
        
      ?>
      
        <td><?php echo $row['userid'];  ?></td>
            <td><?php echo $row['fname']; ?></td>
            <td><?php echo $row['lname']; ?></td>
            <td><?php echo $row['gender']; ?></td>
            <td><?php echo $row['datelogin']; ?></td>
            <td>
            <?php 
            if($row['datelogout'] == NULL){
                ?>
            <form  action="logout.php" method="POST">
                        <input type="hidden" value="<?php echo $row['userid'];?>" name="userid">
                        <input type="hidden" value="<?php echo date('Y-m-d'); ?>" name="logout"/>
                        <input style="cursor:pointer" type="submit" value="LOGOUT">
                    </form> 
                <?php
            } else {
                echo $row['datelogin']; 
            }
                ?>

            </td>
            <td>
            <?php 
            if($row['datelogout'] != NULL){
                $date1 = strtotime(date($row['datelogout']));
                $date2 =  strtotime(date($row['datelogin']));
                $total = $date2 - $date1;
                $datetime = date("s", $total);
                ?>
                  <p><?php echo $datetime; ?> seconds</p>
                <?php
            } 
                ?>
            </td>
            <td>
            <form  action="editpage.php" method="GET">
            <input  type="hidden" value="<?php echo $row['userid'];?>" name="userid">
            <input class="edit" type="submit" value="EDIT">
        </form> 
            </td>
            <td>
            <form  action="delete.php" method="POST">
            <input type="hidden" value="<?php echo $row['userid'];?>" name="userid">
            <input class="delete" type="submit" value="DELETE">
        </form> 
            </td>
        </tr>
      <?php
      }
      ?>
</table>
    </hr>






<p style="text-align:center">List Type</p>
<input class="search" type="text" id="myInput" onkeyup="search()" placeholder="Search for users..">
        <ul class="w3-ul w3-card-4" id="myUL">
      <?php
      include 'connect.php';

      $selectquery = mysqli_query($conn, "SELECT * FROM user");

      while($row= mysqli_fetch_array($selectquery)){
      ?>
       
            <li class="w3-bar">
            <a href="#" style="display:none;"><?php echo $row['userid']; ?></a>
            <a href="#" style="display:none;"><?php echo $row['fname']; ?></a>
            <p>User Id: <?php echo $row['userid'];  ?>
            
        </p>
            <p>First name: <?php echo $row['fname'];  ?></p>
            <p>LAst name: <?php echo $row['lname'];  ?></p>
            <p>Gender: <?php echo $row['gender'];  ?></p>
            <p>Date Login: <?php echo $row['datelogin'];  ?></p>
            <p>
                
            <form style="float: right; margin-left:30px;"  action="editpage.php" method="GET">
            <input  type="hidden" value="<?php echo $row['userid'];?>" name="userid">
            <input class="edit" type="submit" value="EDIT">
        </form> 
        <span style="float: right;">
        <form  action="delete.php" method="POST">
            <input type="hidden" value="<?php echo $row['userid'];?>" name="userid">
            <input class="delete" type="submit" value="DELETE">
        </form> 
      </span>
            </p>
      </li>
      
      <?php
      }
      ?>
</ul>
<!-- </table> -->

<script>

function search() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[1];
       // b = li[i].getElementsByTagName("a")[1];
        txtValue = a.textContent || a.innerText;
       // txtValue2 = b.textContent || b.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
       
    }
}
</script>
</body>
</html>

